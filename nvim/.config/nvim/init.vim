set encoding=utf-8
" Set line numbers
set number
" Line numbers are relative to cursor
set relativenumber
" tabs
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
" Highlight searches
set hlsearch
" Highlight search as you are typing it character-to-character
set incsearch
" Cursor shape
set guicursor=i:block
" autoindent
set autoindent
set ignorecase
set smartcase
set spell spelllang=en_au
filetype plugin on

" Netrw (filebrowser)
let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_altv=1

" used for fuzzy file finder for commands that look through folders.
" For e.g., :find
set path+=**

" remember previous cursor position for each file
if has("autocmd")
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" From Vim-Plug github wiki
" Automatically install missing plugins at startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif

" Plugins installation
" automatically executes `filetype plugin indent on`
call plug#begin()
	" Plug 'fatih/vim-go', { 'tag': '*', 'do': ':GoUpdateBinaries' }
	Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
	Plug 'folke/tokyonight.nvim', { 'branch' : 'main' }
    Plug 'lervag/vimtex'
	Plug 'kaarmu/typst.vim'
call plug#end()


" VimTex configuration
let g:vimtex_view_method = 'zathura'
let g:Tex_MultipleCompileFormats='pdf,bib,pdf'

colorscheme tokyonight-night


" nvim-treesitter configuration
lua << EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = { 
        "c",
        "cpp",
        "cmake",
        "c_sharp",
        "java",
        "bash",
        "ruby",
		"python",
        "javascript",
        "html",
        "make",
        "json",
        "json5",
        "css",
        "diff",
        "gitcommit",
        "gitignore",
        "typescript",
        "vim",
		"rust"
    },
    sync_install = false,
    auto_install = false,
    highlight = {
        enable = true,
        disable = { "latex" },
        additional_vim_regex_highlighting = false,
    },
}
EOF
