" line numbers
set number
set relativenumber "relative line numbers

" tabs and spaces
" tabstop: no. of spaces instead of tab
" shiftwidth: no. of spaces when indented
" expandtab: tabs are replaced with spaces
set tabstop=4 shiftwidth=4 expandtab
filetype indent on
set smarttab
set smartindent    " autoindenting when starting a new line
set autoindent

filetype plugin on
syntax on
set hlsearch
set nocompatible
"set filetype-indent-on
colorscheme pablo
let g:cpp_function_highlight = 0
let g:cpp_attributes_highlight = 1
let g:cpp_member_highlight = 1
let g:cpp_simple_highlight = 1

" remember previous cursor position for each file
if has("autocmd")
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
" filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
" set shellslash

" OPTIONAL: This enables automatic indentation as you type.
" filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'
