#!/usr/bin/env bash


check_cmd() {
    if command -v ${1} &> /dev/null; then
        echo "${1} found"
        return 0
    else
        echo "${1} not found"
        return 1
    fi
}

# just deps for this script directly
# any additional deps for building dwl, somebar, or someblocks need to be checked manually
check_deps() {
    # ninja make git sudo/doas meson
    deps=( "ninja" "make" "git" "meson" )
    for dep in "${deps[@]}"; do
        check_cmd ${dep} # could exit
    done

    if check_cmd "sudo" &>/dev/null || check_cmd "doas" &>/dev/null; then
        echo "sudo/doas found"
    else
        echo "sudo/doas not found"
    fi
}





build_make() {
    make -j${1}
    ${SU_PROG} make install -j${1}
}

main () {
    check_deps

    # set SU_PROG to either doas or sudo
    SU_PROG=$(which doas 2>/dev/null)
    echo "${SU_PROG:=sudo}" &> /dev/null

    # clone all relevant repos
    git clone --branch patches https://gitlab.com/saianeesh/dwl.git
    git clone --branch patches https://gitlab.com/saianeesh/somebar.git
    git clone --branch patches https://gitlab.com/saianeesh/someblocks.git

    # build appropriately
    #   dwl
    cd dwl
    build_make ${1}


    #   somebar
    cd ../somebar
    meson build
    ninja -C build
    ${SU_PROG} ninja -C build install


    #   someblocks
    cd ../someblocks
    build_make ${1}
}

# don't run as root
if [[ $(id -u) -eq 0 ]]; then
    echo "Don't run as root!"
    exit 1
fi


# usage
#   ./dwl_setup n
# where n is the number of cores to use for building/installing
main "${@}"
