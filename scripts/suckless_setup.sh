#!/usr/bin/env bash

# This script builds and installs suckless utilities
# dwm, st, slstatus, and dmenu
# Run this script in a directory in which you want the 
# repositories to clone (i.e. if you run it in . then
# ./dwm ./dwmstatus etc. will be created. 

SU_PRG="doas"




ErrDep() {
    echo "${1} dependancy not met."
    exit 1
}

ErrGitCloning() {
    echo "Error in cloning ${1}."
    exit 1
}

ErrFolderExists() {
    echo "Folder ${1} already exists."
}


IsInstalled() {
    for dep in "$@"; do
        type -p "${dep}" &> /dev/null && continue
        ErrDep "${dep}"
    done
}

# Usage:
#   GitFetch <repo_list> <http/ssh url> [branch] 
GitFetch() {
    local git_base="${2:-git_http_base}"
    local branch="${3:-master}"

    for repo in ${1}; do
        if [[ -d "${repo}" ]]; then
            ErrFolderExists "${repo}"
            continue
        fi
        echo -n "Cloning ${git_base}${repo}..."
        git clone ${git_base}${repo}.git -b ${branch} &> /dev/null

        if [[ $? -ne 0 ]]; then
            ErrGitCloning "${git_base}${repo}.git" && continue
        fi
        echo "done."
        installed+=( "${repo}" )
    done
}

BuildInstall() {
    if [[ -z "$@" ]]; then
        return 0
    fi
    for prog in "$@"; do
        echo -n "Building ${prog}..."
        make -C ${prog} &> /dev/null
        if (( $? != 0 )); then
            echo "ERROR."
        else
            echo "done."
        fi

        echo -n "Installing ${prog}..."
        eval "${SU_PRG} make -C ${prog} install" &> /dev/null
        if (( $? != 0 )); then
            echo "Error."
        else
            echo "Done."
        fi
    done
}

main() {
    local deps=( "git" "make" )
    IsInstalled "${deps[@]}"

    installed=()
    git_http_base="https://gitlab.com/saianeesh/"
    git_ssh_base="git@gitlab.com:saianeesh/"
    git_base=${git_http_base}

    GitFetch "dwm st" "${git_base}" "patches"
    GitFetch "dmenu slstatus" "${git_base}"
    BuildInstall "${installed[@]}"
}

main "$@"
