#!/usr/bin/env bash

# Only for two monitors where the secondary monitor is --right-of the primary monitor

WM=dwm
monitors=$(xrandr --listmonitors | tail -n2 | cut -d':' -f2 | cut -d' ' -f5)
#primary=$(cut -d' ' -f1 <<< ${monitors})
#secondary=$(cut -d' ' -f2 <<< ${monitors})
primary=$(head -n1 <<< ${monitors})
secondary=$(tail -n2 <<< ${monitors})

if (( $(ps -C ${WM} | wc -l) <= 1 )); then
    exit 0
fi

#xrandr --output ${primary} --auto --primary --mode 1366x768 --rate 60.00 --output ${secondary} --auto --mode 1920x1080 --rate 59.93 --scale 1.0001x1 --right-of ${primary};
xrandr-setup() {
    xrandr --output ${1} --auto --primary --mode 1366x768 --rate 60.00 --output ${2} --auto --mode 1920x1080 --rate 59.93 --scale 1.0001x1 --right-of ${1};
}

if (( $(xrandr --listmonitors | wc -l) == 3 )); then
    xrandr-setup ${primary} ${secondary}
fi
