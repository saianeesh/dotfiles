#!/usr/bin/env bash

# Check if script is run as root
if [[ ${UID} -ne 0 ]]; then
    echo "Run this script as root!"
    exit 1
fi

#URL='https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts'
URL='https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling/hosts'

FILENAME="hosts"
TARGET="/etc/hosts" # assuming TARGET is a file, so no "/" at the end
DEPS="curl diff"


check_deps() {
	for dep in ${1}; do
		if [[ ! $(command -v ${dep}) ]]; then
			echo "'${dep}' isn't installed."
			exit 1
		fi
	done
}

download() {
	echo -n "Downloading..."
	curl -Lso ${1} ${2}
	echo "done."
}

main() {
	check_deps "${DEPS}"
	download "${FILENAME}" "${URL}"

	# if /etc/hosts file already exists, create a backup
	if [[ -e "${TARGET}" ]]; then
		# check if there is anything to update
		if [[ -z "$(diff ${TARGET} ${FILENAME})" ]]; then
			echo "Nothing to update."
			rm "./${FILENAME}"
			exit 0
		fi
		echo "${TARGET} -> ${TARGET}.old"
		mv "${TARGET}" "${TARGET}.old"
	fi


	echo "./${FILENAME} -> ${TARGET}"
	mv "./${FILENAME}" "${TARGET}"
	echo "Updated."
}
main
