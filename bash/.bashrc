# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


######################## Environment Variables ########################

# PATH
export PATH=${PATH}:~/.local/bin:/var/lib/flatpak/exports/bin

# XDG
export XDG_CONFIG_HOME=~/.config
export XDG_DOWNLOAD_DIR=~/Downloads
export XDG_SCREENSHOTS_DIR=~/Pictures/Screenshots

# GPG
export GPG_TTY=$(tty)

# SSH-AGENT

# Other
export EDITOR=nvim
export GIT_EDITOR=nvim
#export WINEDLLPATH=/usr/lib/wine/x86_64-windows/:/usr/lib/wine/x86_64-unix/


######################## Other ########################
shopt -s histappend

source ~/dotfiles/scripts/git-prompt.sh


######################## Alias/distribution specific ########################
alias ls='ls --color=auto -Fh'
alias ll='ls -la'
alias pa='ncpamixer'
alias grep='grep --colour=auto'
alias fgrep='fgrep --colour=auto'

which xbps-install &>/dev/null
if (( $? == 0 )); then
    alias xu='doas xbps-install -Su' # update all packages on the system
    alias xc='doas xbps-remove -Oo' # removes obsolete packages and clears cache
    alias xi='doas xbps-install'
    alias xs='doas xbps-query -Rs' # queries the remote repositories
    alias xr='doas xbps-remove'
	eval "$(ssh-agent -s)" &>/dev/null
	if [[ $XDG_SESSION_TYPE == "wayland" ]]; then
		export MOZ_ENABLE_WAYLAND=1 
	fi
	export $(dbus-launch)
fi

which zypper &>/dev/null
if (( $? == 0 )); then
	alias doas='sudo'
    alias zref='sudo zypper ref' # refresh all repositories on the system
    alias zi='sudo zypper install'
    alias zs='sudo zypper search' # queries the remote repositories
    alias zr='sudo zypper rm'
	alias zdup='sudo zypper dup'
fi

######################## Colours  ########################
GIT_PS1_SHOWDIRTYSTATE=1
ASCII_ESC="\[\e[Cm\]"
BLACK=${ASCII_ESC/C/30}
GREEN=${ASCII_ESC/C/32}
CYAN=${ASCII_ESC/C/36}
WHITE=${ASCII_ESC/C/37}
YELLOW=${ASCII_ESC/C/33}
BLUE=${ASCII_ESC/C/34}
LIGHT_GRAY=${ASCII_ESC/C/37}
BG_BLACK=${ASCII_ESC/C/40}
BG_LIGHT_GRAY=${ASCII_ESC/C/47}

RESET=${ASCII_ESC/C/0}
UNDERLINE=${ASCII_ESC/C/4}
UNDERLINE_OFF=${ASCII_ESC/C/24}

# "username path_to_directory $ "
#PS1='\[\e[0m\][\[\e[32m\]\u \[\e[4;36m\]\w\[\e[24m\]\[\e[0m\]\[\e[33m\] $(__git_ps1 "(%s)")\[\e[0m\]]$ '
#PS1="${RESET}[${GREEN}\u${RESET}@${BLUE}\h ${UNDERLINE}${CYAN}\w${UNDERLINE_OFF}${RESET} ${YELLOW}"'$(__git_ps1 "(%s)")'"${RESET}]\$ "
# [username@hostname current_dir ]$ 
PS1="${RESET}[${GREEN}\u${WHITE}@${BLUE}\h ${LIGHT_GRAY}${UNDERLINE}\w${UNDERLINE_OFF} ${YELLOW}"'$(__git_ps1 "(%s)")'"${RESET}]\$ "



######################## Program Settings  ########################
which fff &> /dev/null
if (( $? == 0 )); then
    export FFF_FAV1=~/etc/gitlab/
    export FFF_FAV2=~/etc/
    export FFF_HIDDEN=1
    export FFF_CD_ON_EXIT=1
fi
